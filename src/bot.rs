use lazy_static::lazy_static;
use log::*;
use regex::{
    Regex,
    RegexSet,
    RegexSetBuilder,
};
use serenity::{
    Error,
    model::{
        error::Error::InvalidPermissions,
        Permissions,
        prelude::*,
    },
};

use futures_util::{
    join,
    stream::{
        futures_unordered::FuturesUnordered,
        StreamExt,
    },
};
use reqwest::Client;
use serenity::prelude::*;
use async_trait::async_trait;

const REQUIRED_PERMS: Permissions = Permissions::from_bits_truncate(
    Permissions::READ_MESSAGES.bits() |
    Permissions::SEND_MESSAGES.bits() |
    Permissions::MANAGE_MESSAGES.bits() |
    Permissions::EMBED_LINKS.bits() |
    Permissions::ATTACH_FILES.bits()
);

lazy_static! {
    static ref CW_PATTERN: Regex =
        Regex::new(r"(?i)CW(?:$|[\s:])\s*(.*)").unwrap();
}

pub struct SpoilrsHandler {
    channel_patterns: RegexSet,
    cw_match: bool,
    spoiler_by_react: bool,
}

impl SpoilrsHandler {
    pub fn new(
        channel_patterns: RegexSet,
        cw_match: bool,
        spoiler_by_react: bool
    ) -> SpoilrsHandler {
        SpoilrsHandler {
            channel_patterns,
            cw_match,
            spoiler_by_react,
        }
    }

    pub fn new_from_regex_strings(
        channel_patterns: Vec<String>,
        cw_match: bool,
        spoiler_by_react: bool,
    ) -> Result<SpoilrsHandler, regex::Error> {
        Ok(SpoilrsHandler::new(
            RegexSetBuilder::new(channel_patterns)
                .case_insensitive(true)
                .build()?,
            cw_match,
            spoiler_by_react,
        ))
    }

    pub async fn into_client(self, token: impl AsRef<str>) -> serenity::Result<serenity::Client> {
        serenity::Client::new(token, self).await
    }

    /// Checks to see if a message needs to be cw'd
    ///
    /// This consists of two criteria.  One: There's a reason the message would
    /// need a cw, like being in a cw-mandatory channel, or containing the text
    /// "cw".  Secondly, the message needs to have some media that isn't already
    /// marked as a spoiler.
    ///
    /// If these two criteria are met, then an option is returned, containing
    /// any text that matched the CW.  (e.g. if the message said "CW dogs", the
    /// text "dogs" would be returned.  If these two criteria are not met,
    /// `None` is returned.
    ///
    /// This means that there are three possible return values:
    ///
    ///  - None: The message does not need a cw
    ///  - Some<None>: The message needs a cw, but no cw text was found
    ///  - Some<Some<&str>>: The message needs a cw, and the provided text was
    ///    found
    async fn check_message_needs_cw<'a>(&self, msg: &'a Message, ctx: &Context) -> Option<Option<&'a str>> {
        if let Some(channel) = msg.channel(ctx)
            .await
            .expect("Channel should always be in cache.")
            .guild()
        {
            let cw = CW_PATTERN.captures(&msg.content);
            if (
                // Message labeled with a "CW" in it
                (self.cw_match && cw.is_some()) ||
                // Message in a cw mandatory channel
                self.channel_patterns.is_match(&channel.read().await.name)
            ) && (
                // Message actually has things that need to be changed
                msg.attachments
                    .iter()
                    .any(|a|
                         a.height.is_some() && // Make sure this is an image
                         !a.filename.starts_with("SPOILER") // Not spoilered yet
                    )
                //TODO: Search embeds too
            ) {
                return Some(cw.map(|m| m.get(1).unwrap().as_str()))
            }
        } else {
            // Message sent in DM
        }

        return None
    }

    async fn repost_message(
        &self,
        ctx: &Context,
        msg: &Message,
        cw: Option<&str>,
    ) -> serenity::Result<Message> {

        // Step 0: Verify permissions
        //////////////////////////////////////////////

        let channel_lock = msg.channel_id
            .to_channel_cached(ctx)
            .await
            .expect("Attempt to repost uncached message")
            .guild()
            .expect("Attempt to repost to a private channel");
        let channel = channel_lock.read().await;

        let personal_perms = channel.permissions_for_user(
            ctx,
            ctx.cache.read().await.user.id
        ).await?;

        if !personal_perms.contains(REQUIRED_PERMS) {
            if personal_perms.send_messages() {
                let mut err_response =
                    "Uh oh!  It looks like I'm missing a few permissions that \
                    I need to be able to work!\n\nMissing the following \
                    permissions:\n".to_owned();
                if !personal_perms.attach_files() {
                    err_response.push_str(
                        " - **Attach files**:  So that I can upload newly cw'd \
                        images\n"
                    );
                }
                if !personal_perms.embed_links() {
                    err_response.push_str(
                        " - **Embed Links**:  So that I can credit the \
                        original poster and include their message\n"
                    );
                }
                if !personal_perms.manage_messages() {
                    err_response.push_str(
                        " - **Manage Messages**:  So that I can delete \
                        original messages once I've re-uploaded a cw'd copy\n"
                    );
                }
                err_response.push_str(
                    "\nIf you don't want me in this channel, take away my \
                    persission to **Read Messages** and I'll leave you alone."
                );
                channel.send_message(ctx, |m| m.content(err_response)).await?;
            }
            return Err(Error::Model(InvalidPermissions(
                REQUIRED_PERMS - personal_perms
            )));
        }


        // Step 1: Download all attachments to message
        //////////////////////////////////////////////

        lazy_static! { // Set up reusable client for downloading attachments
            static ref CLIENT: Client = Client::new();
        }
        let cref = &CLIENT; // Needed for async move closure
        // Set up attachment downloads.  Tuple of filename and bytes
        let mut attachments_iter = msg.attachments.iter()
            .map(async move|a| {
                let res: Result<_, reqwest::Error> = Ok((
                    cref
                        .get(&a.url)
                        .send()
                        .await?
                        .bytes()
                        .await?,
                    format!("SPOILER_{}", &a.filename)
                ));
                res
            })
            .collect::<FuturesUnordered<_>>();

        // Await downloads to come in, cleaning off errors and failing fast
        let mut attachments = Vec::with_capacity(msg.attachments.len());
        while let Some(attachment) = attachments_iter.next().await {
            attachments.push(
                attachment.map_err(|e| serenity::Error::Http(
                    Box::new(serenity::http::error::Error::Request(
                        e
                    ))
                ))?
            );
        }

        // Step 2.1: Prepare the new message
        //////////////////////////////////////////////

        let send_repost = channel.send_message(
            ctx,
            |m| m
                .embed(|e| e
                    .author(|a| {
                        if let Some(avi) = msg.author.avatar_url() {
                            a.icon_url(avi)
                        } else {
                            a
                        }.name(&msg.author.name)
                    })
                    .description(&msg.content)
                    .title(
                        if let Some(cw) = cw {
                            format!("CW: {}", cw)
                        } else {
                            format!("Content Warning!")
                        }
                    )
                    .timestamp(&msg.timestamp)
                )
                .add_files(
                    attachments.iter()
                        .map(|bs| (bs.0.as_ref(), bs.1.as_ref()))
                )
        );

        // Step 2.2: Delete the previous message
        //////////////////////////////////////////////

        let delete_message = msg.delete(ctx);

        // Perform steps 2.1 & 2.2 simultaniously
        let (repost_result, delete_result) = join!(send_repost, delete_message);

        delete_result?;

        repost_result
    }
}

#[async_trait]
impl EventHandler for SpoilrsHandler {
    async fn ready(
        &self,
        ctx: Context,
        data: Ready
    ) {
        info!("Logged in as {}!", data.user.name);
        match data.user.invite_url(ctx, REQUIRED_PERMS).await {
            Ok(invite) => {
                info!("Invite me!  {}", invite)
            },
            Err(e) => warn!("Wait no nevermind.  Error: {}", e),
        }
    }

    async fn message(&self, ctx: Context, msg: Message) {
        if let Some(text) = self.check_message_needs_cw(&msg, &ctx).await {
            if let Err(e) = self.repost_message(&ctx, &msg, text).await {
                warn!("Problem trying to repost message {}: {:?}", msg.id, e);
            }
        }
    }

}
