// Copyright 2019 Emi Simpson <emi@alchemi.dev>

// This file is part of the Spoil-rs project.
//
// Spoil-rs is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// Spoil-rs is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with the Pronouner project.  If not, see <https://www.gnu.org/licenses/>.

#![feature(async_closure)]

mod bot;

use clap::{App, Arg};
use confy;
use log::*;
use pretty_env_logger;
use serde::{Deserialize, Serialize};
use tokio;

const DEFAULT_TOKEN: &str = "<Put your Discord token here>";

#[derive(Debug, Serialize, Deserialize, Clone)]
struct Config {
    token: String,
    channel_patterns: Vec<String>,
    cw_match: bool,
    spoiler_by_react: bool,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            token: DEFAULT_TOKEN.to_string(),
            channel_patterns: vec![
                "cw-.*".to_string(),
            ],
            cw_match: true,
            spoiler_by_react: true,
        }
    }
}


#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    debug!("Logger initialized");

    let args = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(Arg::with_name("config")
            .short("c")
            .long("config")
            .value_name("FILE")
            .help(
                "A file with which to configure the bot.  Will be generated if \
                missing.")
            .takes_value(true))
        .arg(Arg::with_name("token")
            .short("t")
            .long("token")
            .value_name("TOKEN")
            .help(
                "The bot token received from Discord.  This overwrites the one \
                provided by the config file if provided."))
        .get_matches();

    let config: Config =
        if let Some(config_path) = args.value_of("config") {
            confy::load_path(config_path).unwrap()
        } else {
            Config::default()
        };

    let token = args.value_of("token")
        .unwrap_or(&config.token);

    if token != DEFAULT_TOKEN {
        // TODO Actually have a bot here
        debug!("Config Received: {:?}", config);
    } else if args.is_present("config") {
        println!(
            "A config has been generated.  Please fill out the token field \
            with a valid Discord bot token, or pass one in with [--token | \
            -t]"
        );
    } else {
        println!(
            "You must either pass in a config with a valid token \
            [--config | -c] or a Discord bot token [--token | -t]"
        );
    }

    bot::SpoilrsHandler::new_from_regex_strings(
        config.channel_patterns,
        config.cw_match,
        config.spoiler_by_react,
    ).unwrap().into_client(token).await.unwrap().start().await.unwrap();
}
